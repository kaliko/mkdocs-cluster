# mkdocs-cluster theme

This project provides an [MkDocs](https://www.mkdocs.org/) Bootstrap theme
heavily based on [mkdocs-bootstrap](http://mkdocs.github.io/mkdocs-bootstrap/)

**Theme demo**: https://kaliko.gitlab.io/mkdocs-cluster/
