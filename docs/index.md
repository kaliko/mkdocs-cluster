# mkdocs-cluster theme

This project provides an [MkDocs] Bootstrap theme
heavily based on [mkdocs-bootstrap](http://mkdocs.github.io/mkdocs-bootstrap/)

__Features__

  * Conditional TOC
  * Hidden pages
  * Put specific pages links in navbar-right

__Libraries:__

 * font awesome 4.6.3
 * bootstrap 3.0.3
 * jquery 1.10.2
 * highlight 9.4.0

__[changelog](changelog.md)__

## Installation

### From pip

```bash
    pip install mkdocs-cluster
```
Then edit your [mkdocs.yml] file and set the [theme]
to `cluster`:

    theme: cluster

### From source

```bash
    # Install from source:
    git clone https://gitlab.com/kaliko/mkdocs-cluster.git cluster
    # fecth web libaries
    cd cluster && sh ./do get_libs
```

After the theme is built, edit your [mkdocs.yml] file and set the [theme_dir]
to `mkdocs_cluster`:

    theme_dir: cluster/mkdocs_cluster

Or you can build/install:
```bash
    # fecth web libaries
    cd cluster && sh ./do get_libs
    ./setup.py install
```

## Theme

### Repo name

Handles GitLab, Bitbucket, GitHub and falls back to [fa-code-fork]

### Table of content

TOC is generated when:

 * there is more than 1 first level header
 * there is unique first level header and more than 1 sub-levels within


For instance:
```markdown
# Title

This will not trigger TOC rendering
```

```markdown
# Title

## SubTitle0
Some text

## SubTitle1
This will trigger TOC rendering
```

```markdown
# Title0

# Title1
Some text

# Title2
This obviously will…
```

## Configuration

Theme configuration is done thanks to extra vars:

```yml
pages:
   - About:   about/index.md
   - Contact: about/contact.md
   - MailUs:  about/mail.md

theme:
    name: cluster
    prev_next_enabled: False
    nav_right: ['About', 'Contact']
    hidden: ['Home', 'MailUs']
    fa_logo: 'fa-cube'
```

 * `prev_next_enabled` : enable previous/next page nav link
 * `nav_right`: pages to put in navbar-right
 * `hidden`: pages to hide from all nav-bar
   Special *Home* value will hide Home link in nav bar, *Home* is the default
   menu entry for index.md when not provided in mkdocs.yml.
 * `fa_logo`: Font Awesome icon as a navbar-brand logo, pick a existing icon from [fontawesome.io]


[Mkdocs]: http://www.mkdocs.org
[mkdics-bootstrap]: http://mkdocs.github.io/mkdocs-bootstrap/
[fa-code-fork]: http://fontawesome.io/icon/code-fork/
[mkdocs.yml]: http://www.mkdocs.org/user-guide/configuration/
[theme]: http://www.mkdocs.org/user-guide/configuration/#theme
[theme_dir]: http://www.mkdocs.org/user-guide/configuration/#theme_dir
[documentation]: http://mkdocs.github.io/mkdocs-bootstrap/
[fontawesome.io]: http://fontawesome.io/icons/

[//]: # ( vim:set syntax=html filetype=markdown spell spelllang=en: )
